# О проекте

## <b>Резпозитории</b>

1) ### <a href="https://gitlab.com/hackKinbery/hackkinbery">Backend</a> - node js сервер 
2) ### <a href="https://gitlab.com/hackKinbery/frontend">Frontend</a> - веб сервис для друзей
3) ### <a href="https://gitlab.com/hackKinbery/mobile">Mobile</a> - мобильный микросервис для поиска ребёнка
4) ### <a href="https://gitlab.com/hackKinbery/kinberyjoint">Описание проекта</a>  
5) ### <a href="https://gitlab.com/hackKinbery/model">Моделирование положения Python</a>  

## Реализованная функциональность

- Беспроводная технология Bluetooth с низким энергопотреблением
- Классификационная модель
- Активные и пассивные проверки. Различные планы энергосбережения


## Особенности проекта

- В случае малой зарядки батареи предлагается сделать систему определения координат по запросу родителей
- Если ребенок гуляет с друзьями, то позиция определяется как усреднение позиций всех друзей
- Построить математическую модель прохождения ребенком стандартных известных маршрутов, например, из школы и в школу, для чего предлагается создать классификационную модель
- В случае необходимости предлагается определять позицию ребенка относительно волонтеров по соединению Bluetooth
 
## Используемые технологии

Server (backend): <b>NodeJS (express js) </b>
<br>CI/CD: <b>GitLab</b>
<br>Mobile ui: <b>React (expo + ble low energy) </b>
<br>Data base: <b>Postgres 13 </b> 
<br>Контейниризация: <b>Docker</b>
<br>Web: <b> Jquery + md ui + google map api </b>
 
## Ссылки

<h3><a href="http://104.248.102.232/">Презентация проекта </a> </h3>
<h3><a href="http://104.248.102.232:8000/api/">Api проекта </a> </h3>
<h3><a href="https://www.figma.com/file/ga24uCU0UzPahXpeQ6KXMD/Kinbery?node-id=65%3A2066">Макеты веб-сайта</a> </h3>
<h3><a href="https://www.figma.com/file/ga24uCU0UzPahXpeQ6KXMD/Kinbery?node-id=49%3A1227">Макеты мобилки</a> </h3>
<h3><a href="https://gitlab.com/hackKinbery/hackkinbery/-/blob/master/README.md">API </a> </h3>

## Установка

Вся информация об запуске в файле <a href="https://gitlab.com/hackKinbery/kinberyjoint/-/blob/main/require.txt">require.txt</a> 

## База данных, выполнение миграции

<a href="https://gitlab.com/hackKinbery/hackkinbery/-/tree/master/backups">Бэкап базы данных</a> нужно восстановить в вашей базе данных, в ней уже находятся все заполненные тестовые данные для тестирования. 

## Команда разработчиков 

<b>Жукова Дарья</b> UI/UX https://t.me/@CAPTAINOFFLINE<br>
<b>Жуков Евгений</b> Devops/Frontend https://t.me/@anybodyseemybeard<br>
<b>Жуков Алексей</b> Frontend https://t.me/@leshik_lesha<br>
<b>Смирнов Данил</b> Backend https://t.me/@whitetech_project<br>
<b>Денисов Артём</b> Analyst/Backend/Architector https://t.me/@rufimych
